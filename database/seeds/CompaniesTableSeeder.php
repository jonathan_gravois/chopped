<?php

use App\Models\Company;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompaniesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('companies')->delete();

        $collection = [
            [
                'company' => 'UAM'
            ],
            [
                'company' => 'ADC'
            ],
            [
                'company' => 'Boeing'
            ],
            [
                'company' => 'Southwest'
            ],
            [
                'company' => 'Jet Blue'
            ],
            [
                'company' => 'Robert Half'
            ]
        ];

        foreach ($collection as $record) {
            Company::create($record);
        }
    }
}
