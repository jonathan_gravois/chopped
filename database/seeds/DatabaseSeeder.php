<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        Model::unguard();

	$seeders = [
		'AddressesTableSeeder',
		'AtasTableSeeder',
		'CompaniesTableSeeder',
		'ConditionsTableSeeder',
		'CountriesTableSeeder',
		'LocationsTableSeeder',
		'MocksTableSeeder',
		'PhonesTableSeeder',
		'ProjectTypesTableSeeder',
		'RolesTableSeeder',
		'UsersTableSeeder',
	];

	foreach($seeders as $seeder){
		$this->call($seeder);
	}

	Model::reguard();
    }
}
