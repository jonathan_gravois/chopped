<?php

Route::get('/', function () {
    return view('pages.home');
});
Route::get('/about', function () {
    return view('pages.about');
});
Route::get('/abouteco', function () {
    return view('pages.abouteco');
});
Route::get('/benefits', function() {
    return view('pages.benefits');
});
Route::get('/certifications', function() {
    return view('pages.certifications');
});
Route::get('/contact', function () {
    return view('pages.contact');
});
Route::get('/customers', function () {
    return view('pages.customers');
});
Route::get('/directors', function () {
    return view('pages.directors');
});
Route::get('/ecobenefits', function () {
    return view('pages.ecobenefits');
});
Route::get('/ecofeatures', function () {
    return view('pages.ecofeatures');
});
Route::get('/ecohome', function () {
    return view('pages.ecohome');
});
Route::get('/faq', function () {
    return view('pages.faq');
});
Route::get('/locations', function () {
    return view('pages.locations');
});
Route::get('/market', function () {
    return view('pages.market');
});
Route::get('/news', function () {
    return view('pages.news');
});
Route::get('/partners', function () {
    return view('pages.partners');
});
Route::get('/pricing', function () {
    return view('pages.pricing');
});
Route::get('/privacy', function () {
    return view('pages.privacy');
});
Route::get('/uploads', function () {
    return view('pages.uploads');
});

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::get('/test', 'AuthenticateController@index');

/* API */
Route::group(['prefix' => 'api'], function() {
    Route::resource('authenticate', 'AuthenticateController', ['only' => ['index']]);
    Route::post('authenticate', 'AuthenticateController@authenticate');
    Route::get('authenticate/user', 'AuthenticateController@getAuthenticatedUser');

    Route::resource('addresses', 'AddressesController');
    Route::resource('companies', 'CompaniesController');
    Route::resource('conditions', 'ConditionsController');
    Route::resource('countries', 'CountriesController');
    Route::resource('locations', 'LocationsController');
    Route::resource('mocks', 'MocksController');
    Route::resource('phones', 'PhonesController');
    Route::resource('roles', 'RolesController');
    Route::resource('users', 'UsersController');

    Route::get('atas', 'AtasController@index');
    Route::get('atas/chapters', 'AtasController@chapters');
});