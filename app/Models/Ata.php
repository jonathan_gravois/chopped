<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ata extends Model
{
    public $timestamps = false;
    protected $table = 'atas';
    protected $fillable = [
		'id',
		'chapter',
		'section',
		'title',
		'description',
		'created_at',
		'updated_at',
	];

    /* CASTING */
    protected $casts = [
		'id' => 'integer',
	];
    /* CASTING */

    /* RELATIONSHIPS */
    /* RELATIONSHIPS */

    /* METHODS */
    /* METHODS */
}
