<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Part extends Model
{
    /*public $timestamps = false;*/
    protected $table = 'parts';
    protected $fillable = [
		'id',
		'ata_chapter',
		'base_part_number',
		'description',
	];
    protected $hidden = ['created_at', 'updated_at'];

    /* CASTING */
    protected $casts = [];
    /* CASTING */

    /* RELATIONSHIPS */
    /* RELATIONSHIPS */

    /* METHODS */
    /* METHODS */
}