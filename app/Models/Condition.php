<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Condition extends Model
{
    public $timestamps = false;
    protected $table = 'conditions';
    protected $fillable = [
		'id',
		'code',
		'description',
		'created_at',
		'updated_at',
	];

    /* CASTING */
    protected $casts = [
		'id' => 'integer',
	];
    /* CASTING */

    /* RELATIONSHIPS */
    /* RELATIONSHIPS */

    /* METHODS */
    /* METHODS */
}
