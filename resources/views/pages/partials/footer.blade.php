<div class="container">
    <div class="row">
        <!-- Link List -->
        <div class="col-md-12 md-margin-bottom-40">
            <ul class="list-menu">
                <li><a href="/">Home</a></li>
                <li><a href="/about">About us</a></li>
                <li><a href="/customers">Customers</a></li>
                <li><a href="/market">ADC Marketplace</a></li>
                <li><a href="/contact">Contact Us</a></li>
                <li><a href="/faq">FAQ</a></li>
                <li><a href="/privacy">Privacy Policy</a></li>
            </ul>
            <br>
            <p class="text-center copyright">
                &copy; {{date("Y")}} Areospace Disassembly Consortium. All Rights Reserved. <a href="#">Terms of Service</a>.
            </p>
        </div><!--/col-md-3-->
        <!-- End Link List -->
    </div>
</div>
